class TriggerType {
  /**
   * @param {string} name 
   */
  constructor (name) {
    this.name = name;
  }
}

const TriggerTypes = {
  VendorPoll: new TriggerType('vendor-poll'),
  VendorEvent: new TriggerType('vendor-event'),
  P17Poll: new TriggerType('p17-poll'),
  P17Event: new TriggerType('p17-event'),
}

module.exports = {
  TriggerType,
  TriggerTypes
}