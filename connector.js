const { IntegrationStatus } = require('@pipe17/api-client');
const { API } = require('./api');
const { ConnectorContext } = require('./context');
const logger = require('./common/logger').defaultLogger.child({ module: 'p17-con' });

/**
 * @typedef {ConnectorContext} Context
 */

/**
 * @typedef {function(Context): any | any[] | Promise<any> | Promise<any[]>} initHandler 
 */

/**
 * @param {Object[]} actions 
 */
async function _exec(actions) {
  const result = [];
  for (let action of actions) {
    const r = await action.exec();
    result.push(r);
  }
  return result;
}

class Connector {
  /** 
   * @param {import('./configuration').Configuration} configuration
   */
  constructor(configuration) {
    logger.info('ctor', { configuration });
    this.configuration = configuration;
  }
  /**
   * @param {import('./context').Args} args 
   * @param {initHandler} initCallback
   */
  async exec(args, initCallback) {
    logger.info('exec', { args });

    const context = new ConnectorContext(this);
    const prepared = await context.prepare(args);

    if (initCallback) {
      await initCallback(prepared);
    }
    
    if (prepared.integration && prepared.integration.status != IntegrationStatus.Connected) {
      logger.info('wait initialization', { status: prepared.integration.status });
      return {};
    }
    if (prepared.integration && args.entityTypesSettings) {
      const entityTypesSettings = args.entityTypesSettings;
      const settings = prepared.configuration.settings;
      prepared.entityTypes = prepared.entityTypes.filter((e) => {
        const key = entityTypesSettings[e];
        return !key || settings.getValue(key);
      });
    }

    const entities = prepared.configuration.entities;
    const actions = entities.getActions(prepared);
    const result = await _exec(actions);
    return result;
  }

  /**
   * @param {import('./context').Context} context 
   * @param {string} query 
   * @param {string} entityType 
   * @param {string} direction 
   * @return {string|undefined}
   */
  async lastTimestamp(context, query, entityType, direction) {
    query;
    // exclude timestamps for entityType:integrations
    if (entityType === 'integrations') {
      return;
    }
    this._timestampCache = this._timestampCache || {};

    const key = entityType + '-' + direction + '-updated';
    if (this._timestampCache[key]) {
      return this._timestampCache[key];
    }
    const result = await context.api.integration_state.fetch(key);
    const timestamp = result || new Date("2020-01-01").toISOString();

    this._timestampCache[key] = timestamp;
    return timestamp;
  }
  /**
   * @param {import('./context').Context} context 
   * @param {string} entityType 
   * @param {string} direction 
   * @param {bool} force 
   * @param {string} timestamp 
   */
  async setTimestamp(context, entityType, direction, index, force, timestamp) {
    // exclude timestamps for entityType:integrations
    if (entityType === 'integrations') {
      return;
    }
    if (!timestamp) {
      return;
    }
    const last = await this.lastTimestamp(context, {}, entityType, direction).catch((e) => new Date("2020-01-01").toISOString());
    this._timestampCache = this._timestampCache || {};
    const key = entityType + '-' + direction + '-updated';

    if (timestamp.toISOString) {
      timestamp = timestamp.toISOString();
    }

    if (timestamp > last) {
      this._timestampCache[key] = timestamp;
    }
    timestamp = this._timestampCache[key];

    if (!force && ((index % 10) !== 0)) {
      return;
    }

    if (force) {
      timestamp = new Date(new Date(timestamp).valueOf() + 1).toISOString();
    }
    return await context.api.integration_state.update(key, timestamp);
  }

  async convert(context, item, entityType, direction) {
    const mapping = context.configuration.mappings.findMapping(entityType, direction);
    if (mapping) {
      const converted = await context.api.schema.convertWithJSON(item, mapping.mappingRules);
      //converted.__source = item;
      return converted;
    }
    return item;
  }
}

module.exports = {
  Connector,
}