const AWS = require("aws-sdk");
const random = require('../utils').random;
const _logger = require('../logger').defaultLogger;

const logger = _logger.child({ module: 'aws-dynamodb' });

const insert = async (tableName, keyName, item) => {  
  logger.info('insert', { tableName, keyName, item });

  if (!item.id) {
    item.id = random.newObjectId();
  }
  item.created = new Date().toISOString();
  item.updated = item.created;
  const docClient = new AWS.DynamoDB.DocumentClient();
  const params = {
    TableName: tableName,
    Item: item
  };

  try {
    const result = await docClient.put(params).promise();
    logger.info('insert:done', { result, tableName, keyName, item });
    return item[keyName];
  } catch (err) {
    logger.error('insert:fail', { err, tableName, keyName, item });
    throw err;
  }  
}

const remove = async (tableName, keyName, id) => {
  logger.info('remove', { tableName, keyName, id });

  const docClient = new AWS.DynamoDB.DocumentClient();
  const params = {
    TableName: tableName,
    Key: {
      [keyName]: id
    },
    ConditionExpression: keyName + " = " + ":" + keyName,
    ExpressionAttributeValues: {        
      [":" + keyName]: id,
    },
  };

  try {
    const result = await docClient.delete(params).promise();
    logger.info('remove:done', { result, tableName, keyName, id });
    return true;
  } catch (err) {
    if (err.code === 'ConditionalCheckFailedException') {
      return false;
    }
    logger.error('remove:fail', { err, tableName, keyName, id });
    throw err;
  }  
}

const update = async (tableName, keyName, item) => {
  logger.info('update', { tableName, keyName, item });

  const docClient = new AWS.DynamoDB.DocumentClient();
  const params = {
      TableName: tableName,
      Key: { 
        [keyName] : item[keyName]
      },
      ConditionExpression: keyName + " = " + ":" + keyName,
      ExpressionAttributeValues: {        
        [":" + keyName]: item[keyName],
      },
      ExpressionAttributeNames: {},
      UpdateExpression: '',
      ReturnValues: "UPDATED_NEW"
  };

  item.updated = new Date().toISOString();
  Object.getOwnPropertyNames(item).forEach((prop, index_) => {
    if (prop === keyName) {
      return;
    }
    const prefix = (params.UpdateExpression.length === 0) ? 'set ' : ', '
    params.UpdateExpression += prefix + '#' + prop + ' = :' + prop;
    params.ExpressionAttributeValues[':' + prop] = item[prop];
    params.ExpressionAttributeNames['#' + prop] = prop;
  })

  try {
    const result = await docClient.update(params).promise();
    logger.info('update:done', { result, tableName, keyName, item });
    return true;
  } catch (err) {
    if (err.code === 'ConditionalCheckFailedException') {
      return false;
    }
    logger.error('update:fail', { err, tableName, keyName, item });
    throw err;
  }  
}

const fetch = async (tableName, keyName, id) => {
  logger.info('fetch', { tableName, keyName, id });

  const docClient = new AWS.DynamoDB.DocumentClient();
  const params = {
    TableName: tableName,
    Key: {
      [keyName]: id
    },
  };

  try {
    const result = await docClient.get(params).promise();
    logger.info('fetch:done', { result, tableName, keyName, id });
    if (!result.Item) {
      return null;
    }
    return result.Item;
  } catch (err) {
    logger.error('fetch:fail', { err, tableName, keyName, id });
    throw err;
  }  
}

const last = async (tableName, sortName, since) => {  
  logger.info('last', { tableName, sortName, since });

  const docClient = new AWS.DynamoDB.DocumentClient();
  const params = {
    TableName: tableName,
    IndexName : "valid-index",
    KeyConditionExpression: "valid = :valid",    
    ExpressionAttributeValues: {        
      ":valid": "true",
    }
  };
  if (since) {
    const sortAttrName = ":" + sortName;
    params.KeyConditionExpression += " AND " + sortName + " >= " + sortAttrName;
    params.ExpressionAttributeValues[sortAttrName] = since;
  }

  const query  = async (params, result = []) => {
    const data = await docClient.query(params).promise();
  
    if (data.Items.length > 0) {
      result = [...result, ...data.Items]
    }
  
    if (data.LastEvaluatedKey) {
      logger.info('last:querymore', {data, tableName, sortName, since });        
      params.ExclusiveStartKey = data.LastEvaluatedKey
      return await query(params, result)
    } else {
      return result
    }
  }

  try {
    const result = await query(params);
    logger.info('last:done', { result, tableName, sortName, since });
    return result;
  } catch (err) {
    logger.error('last:fail', { err, tableName, sortName, since });
    throw err;
  }  
}

const createTable = async (tableName, keyName) => {
  logger.info('createTable', { tableName, keyName });

  const dynamodb = new AWS.DynamoDB();
  const params = {
    TableName : tableName,
    KeySchema: [       
      { AttributeName: keyName, KeyType: "HASH"},            
    ],
    AttributeDefinitions: [       
      { AttributeName: keyName, AttributeType: "S" },          
    ],
    ProvisionedThroughput: {       
      ReadCapacityUnits: 5, 
      WriteCapacityUnits: 5
    }
  };
  
  try {
    const result = await dynamodb.createTable(params).promise();
    logger.info('createTable', { result, tableName, keyName });
  } catch (err) {
    if (err.code === 'ResourceInUseException') {
      logger.warn('createTable', { err, tableName, keyName });
    } else {
      logger.error('createTable:fail', { err, tableName, keyName });
      throw err;
    }
  }
}

module.exports = {  
  insert,
  fetch,
  remove,
  update,
  last,
  createTable
}