const dynamoDB = require('./dynamodb');
const lambda = require('./lambda');

module.exports = {
  dynamoDB,
  lambda
}