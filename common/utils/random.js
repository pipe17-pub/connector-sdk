const crypto = require('crypto')

const randomString = (size, digits) => {
  if (size === 0) {
    return '';
  }

  let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' + 'abcdefghijklmnopqrstuvwxyz';
  if (digits) {
    chars += '0123456789';
  }

  let result = '';
  const bytes = crypto.randomBytes(size);
  for (let i = 0; i < bytes.length; ++i) {
    result += chars[bytes.readUInt8(i) % chars.length];
  }
  return result;
}

const randomNumber = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const newObjectId = () => {
  return randomString(10, true);
}

module.exports = {   
  randomString,
  randomNumber,
  newObjectId
}
