const dynamoDB = require('../aws').dynamoDB;
const faker = require('faker');

const TableName = process.env.P17SDK_MOCK_ORDERS_TABLE_NAME;
const TableKey = process.env.P17SDK_MOCK_ORDERS_TABLE_KEY_NAME;
const TableSort = process.env.P17SDK_MOCK_ORDERS_TABLE_SORT_NAME;

const generate = () => {
  return {
    [TableKey] : faker.random.alphaNumeric(10),
    "created": new Date().toISOString(),    
    "updated": "",    
    "status": "New",
    "name" : faker.helpers.replaceSymbolWithNumber("ORD-#####"),
    "total": total = faker.random.number({ min:1, max:10000, precision:2}),    
    "tax": +(total * 0.18).toFixed(2),    
    "items" : 
      [...Array(faker.random.number({ min:1, max:10})).keys()].map(o => {
        return {
          "id": faker.random.alphaNumeric(10), 
          "sku": faker.finance.bic(), 
          "title": faker.commerce.productName()
        }
      }),
    "shipping_address": {
      "firstName": faker.name.firstName(),
      "lastName": faker.name.lastName(),
      "address": faker.address.streetAddress(),
      "address2": faker.address.secondaryAddress(),
      "city": faker.address.city(),
      "state": faker.address.state(),
      "zip": faker.address.zipCode(),
      "country": faker.address.countryCode()
    },
    "billing_address": {
      "firstName": faker.name.firstName(),
      "lastName": faker.name.lastName(),
      "address": faker.address.streetAddress(),
      "address2": faker.address.secondaryAddress(),
      "city": faker.address.city(),
      "state": faker.address.state(),
      "zip": faker.address.zipCode(),
      "country": faker.address.countryCode()
    },
    "shipping_carrier": {
      "name": faker.company.companyName(),    
      "phone": faker.phone.phoneNumber(),    
      "email": faker.internet.email(),    
      "address": faker.address.streetAddress(true),    
    },
    "valid": "true",
  };
}

const insert = dynamoDB.insert.bind(null, TableName, TableKey);

const fetch = dynamoDB.fetch.bind(null, TableName, TableKey);

const remove = dynamoDB.remove.bind(null, TableName, TableKey);

const update = dynamoDB.update.bind(null, TableName, TableKey);

const last = dynamoDB.last.bind(null, TableName, TableSort);

module.exports = {  
  generate,
  insert,
  fetch,
  remove,
  update,
  last
}
