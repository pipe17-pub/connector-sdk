const dynamoDB = require('../aws').dynamoDB;
const random = require('../utils').random;

const TableName = process.env.P17SDK_MOCK3PL_SHIPMENTS_TABLE_NAME;
const TableKey = process.env.P17SDK_MOCK3PL_SHIPMENTS_TABLE_KEY_NAME;
const TableSort = process.env.P17SDK_MOCK3PL_SHIPMENTS_TABLE_SORT_NAME;

const generate = () => {
  return {
    [TableKey] : random.newObjectId(),
    "created": new Date().toISOString(),    
    "updated": "",    
    "status": "New",
    "shipmentId" : random.newObjectId(),
    "valid": "true",
  };
}

const insert = dynamoDB.insert.bind(null, TableName, TableKey);

const fetch = dynamoDB.fetch.bind(null, TableName, TableKey);

const remove = dynamoDB.remove.bind(null, TableName, TableKey);

const update = dynamoDB.update.bind(null, TableName, TableKey);

const last = dynamoDB.last.bind(null, TableName, TableSort);

module.exports = {
  generate,
  insert,
  fetch,
  remove,
  update,
  last
}
