const orders = require('./orders')
const shipments = require('./shipments')
const shipments3pl = require('./shipments3pl')

const { OrdersAPI, ShipmentsAPI } = require('./api');

module.exports = {
  orders,
  shipments,
  shipments3pl,

  OrdersAPI, ShipmentsAPI
}
