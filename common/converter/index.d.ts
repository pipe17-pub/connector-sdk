export interface ConverterConfig {
    url: string;
    token: string;
    debug?: boolean;
}
export declare type ConverterMappingId = string;
export declare type ConverterLookup = Map<string, string>;
export interface ConverterMapping {
    name: string;
    enabled: boolean;
    source: string;
    target: string;
    lookup?: ConverterLookup;
    missing?: string;
    default?: string;
}
export interface ConverterRules {
    mappings: Array<ConverterMapping>;
}
export interface ConverterRequest<T> {
    rules: ConverterRules | ConverterMappingId;
    source: T;
}
export interface ConverterResult<T> {
    status: number;
    body: T;
}
export interface ConverterResponse<T> {
    message: string;
    result: ConverterResult<T>;
    info: any;
}
export declare class ConverterError extends Error {
    name: "ConverterError";
    status: number;
    text: string;
    requestId: string;
    constructor(message: string, status?: number, requestId?: string, text?: string);
}
export declare class Converter {
    readonly config: ConverterConfig;
    constructor(config: ConverterConfig);
    convert<T, U>(rules: ConverterMappingId | ConverterRules, source: T): Promise<U>;
}
