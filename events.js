const uuid = require('uuid');
const logger = require('./common/logger').defaultLogger.child({ module: 'p17-con' });

const { EventRequestStatus } = require('@pipe17/api-client');

class ConnectorEvents {

  /**
   * @param {import('./context').ConnectorContext} context 
   */
  constructor(context) {
    this.context = context;
  }

  /**
   * @param {object} info 
   * @param {string} info.operation 
   * @param {string} info.entityType 
   * @param {string} info.entityId 
   * @param {object} info.entityDetails 
   * @param {string} info.errorText 
   * @param {string} info.exceptionType 
   * @param {object} info.response 
   * @param {boolean} info.completed 
   */
  async reportEvent(info) {
    const requestId = this.context.args.requestId;
    const integrationId = this.context.args.integrationId;

    const body = {
      entityId: info.entityId,
      entityType: info.entityType,
      operation: info.operation,
    }
    const response = info.response || {
      status: 200,
      message: 'OK',
      headers: {},
      body: ''
    }

    /** @type {import('@pipe17/api-client').Event} */
    const event = {
      source: 'custom',
      requestId: this.context.args.requestId || uuid.v4(),
      timestamp: new Date().toISOString(),

      operation: info.operation,
      entityType: info.entityType,
      entityId: info.entityId,

      integration: integrationId, // ?? should be implied

      request: {
        url: `/connector-events/${integrationId}`,  // TODO: expose connector endpoint for retries
        method: 'POST',
        headers: {}, // TODO: generate connector request signature for retries
        authobj: undefined, // not required
        body: JSON.stringify(body)
      },

      response: response
    };

    // in case of 2 phase report, rely to last event, since we are processing sequentially
    if (this._lastEvent && (this._lastEvent.entityId === event.entityId) && (this._lastEvent.entityType === event.entityType) && (this._lastEvent.operation === event.operation)) {
      event.eventId = this._lastEvent.eventId;
    }

    if (!info.completed && !info.errorText) {
      event.status = EventRequestStatus.Pending;
    } else if (info.errorText) {
      event.status = EventRequestStatus.Failed;
      event.response = info.response || {
        status: 500,
        message: info.errorText,
      }
    } else {
      event.status = EventRequestStatus.Completed;
      event.response = info.response || {
        status: 200,
        message: 'OK',
      }
    }

    let dfd = Promise.resolve();
    if (info.exceptionType) {
      /** @type {import('@pipe17/api-client').Exception} */
      const exception = {
        entityId: info.entityId,
        entityType: info.entityType,
        exceptionType: info.exceptionType,
        exceptionDetails: info.errorText
      }
      dfd = this.reportException(exception).catch(() => null);
    }

    logger.info('report:event', { requestId, integrationId, event });
    const result = await this.context.api.events.upsert(event).catch((error) => {
      logger.error('report:event:failed', { requestId, integrationId, event, error });
    });

    this._lastEvent = null;
    if (result) {
      this._lastEvent = event;
      event.eventId = result.eventId;
    }

    // wait for exception creation if any
    await dfd;

    logger.info('report:event:done', { requestId, integrationId, eventId: event.eventId });
    return event;
  }

  /**
   * @param {import('@pipe17/api-client').Exception} exception 
   */
  async reportException(exception) {
    const requestId = this.context.args.requestId;
    const integrationId = this.context.args.integrationId;

    logger.info('report:exception:done', { requestId, integrationId, exception });

    const result = await this.context.api.exceptions.upsert(exception).catch((error) => {
      logger.error('report:exception:failed', { requestId, integrationId, exception, error });
    });
    if (result) {
      exception.exceptionId = result.exceptionId;
    }

    logger.info('report:exception:done', { requestId, integrationId, exceptionId: exception.exceptionId });
    return exception;
  }
}

module.exports = {
  ConnectorEvents
}