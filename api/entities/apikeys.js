const { APIAccessor } = require('../accessor');
APIAccessor;

class APIKeys {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }
}

module.exports = {
  APIKeys
}