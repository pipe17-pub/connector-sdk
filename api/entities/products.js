const { ProductCreateResultStatusEnum } = require('@pipe17/api-client');
const { APIAccessor, chunk, logger } = require('../accessor');
APIAccessor;

class Products {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }

  async * iterator(opt) {
    opt = opt || { };
    opt.skip = opt.skip || 0;
    opt.count = opt.count || 50;
    logger.info('products:iterator', { opt });

    const hash = { current: {}, previous: {} };
    while (!opt._stop) {
      const products = await this.list(opt);

      let updatedAt = null;
      const filtered = products.filter((x) => {
        const id = x.productId;
        if (hash.previous[id]) {
          return false;
        }
        if (!updatedAt || (updatedAt < x.updatedAt)) {
          updatedAt = x.updatedAt;
        }
        hash.current[id] = 1;
        return true;
      });

      if (opt.batch) {
        if (filtered.length) {
          const report = {
            items: filtered,
            updatedAt: updatedAt
          }
          const r = await (yield report);
          r;
        }
      } else {
        for (let product of filtered) {
          const r = await (yield product);
          r;
        }
      }
      
      hash.previous = hash.current;
      hash.current = {};

      if (opt._next) {
        opt._next(opt, products);
      } else if (opt._nextAsync) {
        await opt._nextAsync(opt, inventory)
      } else if (opt.order === 'updatedAt') {
        const last = products[products.length - 1] || {};
        opt.updatedSince = last.updatedAt;
        opt.skip = 0;
      } else {
        opt.skip = opt.skip + opt.count;
      }

      opt._stop = opt._stop || (products.length < opt.count);
    }

    logger.info('products:iterator:done');
  }
  async list(opt) {
    opt = opt || { };
    logger.info('products:list', { opt });
    const result = await this.raw_api.listProducts({ ...opt }).catch((error) => {
      logger.error('products:list:failed', { opt, error });
      return Promise.reject(error);
    });
    const products = result.products;
    logger.info('products:list:done', { products });
    return products;
  }
  /**
   * @param {string|{ productId: string }} id
   */
  async fetch(id) {
    const query = (typeof (id) === 'string') ? { productId: id } : id ;
    logger.info('products:fetch', { query });
    const result = await this.raw_api.fetchProduct(query).catch((error) => {
      if (error && error.failure && error.failure.code === 404) {
        return { product: null };
      }
      logger.error('products:fetch:failed', { query, error });
      return Promise.reject(error);
    });
    const product = result.product;
    logger.info('products:fetch:done', { product });
    return product;
  }

  /**
   * @param {import('@pipe17/api-client').Product} item
   */
  async upsert(item) {
    const products = (item.products) ? item.products : [item];

    if (products.length > 1) {
      logger.info('products:upsert', { count: products.length });
      const chunked = chunk(products);
      const promises = chunked.map((c, index) => {
        logger.info('products:upsert:chunk', { index, count: c.length });
        return this.raw_api.createProducts({
          productCreateData: c
        }).then((result) => {
          const failed = result.products.filter(product => product.status == ProductCreateResultStatusEnum.Failed);
          logger.info('products:upsert:chunk:done', { index, count: c.length, failed: failed.length });
          const promises = failed.map((x) => {
            logger.info('products:upsert:update', { index, count: c.length });
            return this.raw_api.updateProduct({
              productId: x.product.productId,
              productUpdateRequest: x.product
            }).catch((error) => {
              logger.error('products:upsert:update:failed', { index, count: c.length, error });
              return Promise.reject(error);
            });
          })
          return Promise.all(promises);
        }).catch((error) => {
          logger.error('products:upsert:create:failed', { index, count: c.length, error });
        });
      });
      return Promise.all(promises);
    }

    logger.info('products:upsert', { item });

    const query = {};
    if (item.extProductId) {
      query.productId = 'ext:' + item.extProductId;
    } else {
      query.productId = 'sku:' + item.sku;
    }
    const existing = await this.fetch(query).catch(() => null); 
    if (existing) {
      item.productId = existing.productId;
    }

    if (!item.productId) {
      const result = await this.raw_api.createProducts({
        productCreateData: [item]
      }).catch((error) => {
        logger.error('products:create:failed', { error });
        return Promise.reject(error);
      });
      item.productId = result.products[0].product.productId;
    } else {
      const result = await this.raw_api.updateProduct({
        productId: item.productId,
        productUpdateRequest: item,
      }).catch((error) => {
        logger.error('products:update:failed', { error });
        return Promise.reject(error);
      });
      result;
    }

    logger.info('products:upsert:done', { item });
    return item;
  }
}

module.exports = {
  Products
}