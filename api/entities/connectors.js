const { APIAccessor, logger } = require('../accessor');
APIAccessor;

class Connectors {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }

  async list(name) {
    logger.info('connectors:list', { name: name });
    /** @type {api.ConnectorsListResponse} */
    const result = await this.raw_api.listConnectors({ connectorName: name }).catch((error) => {
      if ((error.isP17Failure) && (error.failure.code === 404)) {
        return { connectors: [] };
      }
      throw error
    });
    const connectors = result.connectors;
    logger.info('connector:list:done', { connectors: connectors.length });
    return connectors;
  }
  async fetch(id) {
    logger.info('connectors:fetch', { id });
    /** @type {api.ConnectorFetchResponse} */
    const result = await this.raw_api.fetchConnector({ connectorId: id }).catch((error) => {
      if ((error.isP17Failure) && (error.failure.code === 404)) {
        return { connector: null };
      }
      throw error
    });
    const connector = result.connector;
    logger.info('connector:fetch:done', { id: (connector || {}).connectorId });
    return connector;
  }
  /**
   * @param {api.ConnectorCreateRequest} connectorInfo 
   */
  async create(connectorInfo) {
    logger.info('connectors:create', { name: connectorInfo.name });
    /** @type {api.ConnectorFetchResponse} */
    const result = await this.raw_api.createConnector({ 
      connectorCreateRequest: connectorInfo
    });
    const connector = result.connector;
    logger.info('connector:create:done', { id: (connector || {}).connectorId });
    return connector;
  }
  /**
   * @param {api.Connector} connectorInfo
   */
  async update(connectorInfo) {
    logger.info('connectors:update', { id: connectorInfo.connectorId, name: connectorInfo.name });
    /** @type {api.ConnectorUpdateResponse} */
    const result = await this.raw_api.updateConnector({ 
      connectorId: connectorInfo.connectorId,
      connectorUpdateRequest: connectorInfo
    });
    const success = result.success;
    logger.info('connector:update:done', { success, id: connectorInfo.connectorId });
    return connectorInfo;
  }
  async remove(id) {
    logger.info('connectors:remove', { id: id });
    /** @type {api.Success} */
    const result = await this.raw_api.deleteConnector({ connectorId: id });
    const success = result.success;
    logger.info('connectors:remove:done', { success, id: id });
    return success;
  }
}

module.exports = {
  Connectors
}