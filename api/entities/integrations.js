const { APIAccessor, logger } = require('../accessor');
APIAccessor;
const api = require('@pipe17/api-client');
api;

class Integrations {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }
  async * iterator(opt) {
    opt = opt || { };
    opt.skip = opt.skip || 0;
    opt.count = opt.count || 50;
    logger.info('integrations:iterator', { opt });

    while (!opt.stop) {
      const integrations = await this.list(opt);
      opt.skip = opt.skip + opt.count;
      for (let integration of integrations) {
        yield integration
      }
      if (integrations.length < opt.count) {
        break;
      }
    }

    logger.info('integrations:iterator:done');
  }
  async list(opt) {
    opt = opt || { };
    logger.info('integrations:list', { opt });
    const result = await this.raw_api.listIntegrations({ ...opt }).catch((error) => {
      logger.error('integrations:list:failed', { opt, error });
      return Promise.reject(error);
    });
    const integrations = result.integrations;
    logger.info('integrations:list:done', { integrations });
    return integrations;
  }
  /**
   * @param {string|{ integrationId: string }} id
   */
  async fetch(id) {
    const query = (typeof (id) === 'string') ? { integrationId: id } : id ;
    logger.info('integrations:fetch', { query });
    const result = await this.raw_api.fetchIntegration(query).catch((error) => {
      logger.error('integrations:fetch:failed', { query, error });
      return Promise.reject(error);
    });
    const integration = result.integration;
    logger.info('integrations:fetch:done', { query });
    return integration;
  }

  /**
   * @param {api.Integration} item 
   */
  async create(item) {
    logger.info('integrations:create', { item });
    const result = await this.raw_api.createIntegration({ 
      integrationCreateRequest: item 
    }).catch((error) => {
      logger.error('integrations:create:failed', { item, error });
      return Promise.reject(error);
    });
    const integration = result.integration;
    logger.info('integrations:create:done', { item });
    return integration;
  }

  /**
   * @param {api.Integration} item 
   */
  async update(item) {
    logger.info('integrations:update', { item });
    const result = await this.raw_api.updateIntegration({ 
      integrationId: item.integrationId, 
      integrationUpdateRequest: item
    }).catch((error) => {
      logger.error('integrations:update:failed', { item, error });
      return Promise.reject(error);
    });
    const success = result.success
    logger.info('integrations:update:done', { success, item });
    return item;
  }

  /**
   * @param {string} id 
   * @param {object} [info] 
   */
  async upgrade(id, info = {}) {
    logger.info('integrations:upgrade', { id, info });
    const result = await this.raw_api.upgradeIntegration({ 
      integrationId: id, 
      integrationUpgradeRequest: info
    }).catch((error) => {
      logger.error('integrations:upgrade:failed', { id, error });
      return Promise.reject(error);
    });
    const success = result.success
    logger.info('integrations:upgrade:done', { success, id });
    return result;
  }

  /**
   * @param {string|{ integrationId: string }} id 
   */
  async remove(id) {
    const query = (typeof (id) === 'string') ? { integrationId: id } : id ;
    logger.info('integrations:remove', { id });
    const result = await this.raw_api.deleteIntegration(query).catch((error) => {
      logger.error('integrations:remove:failed', { id, error });
      return Promise.reject(error);
    });
    const success = result.success
    logger.info('integrations:remove:done', { success, id });
    return success;
  }
}

module.exports = {
  Integrations
}