const { APIAccessor, logger } = require('../accessor');
APIAccessor;
const api = require('@pipe17/api-client');
api;

class Mappings {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }

  /**
   * @param {(api.Mapping & { mappingRules: any })[]} mappings
   * @param {boolean} isPublic
   * @param {string} description
   */
  async updateBatch(mappings, isPublic, description) {
    const connectorName = '';
    logger.info('mappings:update', { connectorName, count: mappings.length });

    for (let mapping of mappings) {
      let _mapping = await this.fetch(mapping.mappingUuid).catch((e) => null);
      if (!_mapping) {
        _mapping = await this.create({
          uuid: mapping.mappingUuid,
          isPublic: isPublic,
          mappings: mapping.mappingRules,
          description: `${description}:${mapping.name}:${mapping.direction}`
        })
      } else {
        // todo: compare mappings;
        _mapping.mappings = mapping.mappingRules;
        _mapping = await this.update(_mapping);
      }
    }

    logger.info('mappings:update:done', { connectorName, count: mappings.length });
    return mappings;
  }

  async fetch(uuid) {
    logger.info('mappings:fetch', { uuid });
    /** @type {api.MappingFetchResponse} */
    const result = await this.raw_api.fetchMapping({ mappingId: "uuid:" + uuid }).catch((error) => {
      logger.error('mappings:fetch:failed', { uuid, error });
      return Promise.reject(error);
    });
    const mapping = result.mapping;
    logger.info('mappings:fetch:done', { id: (mapping || {}).mappingId, uuid: uuid });
    return mapping;
  }
  /**
   * @param {api.Mapping} mapping 
   */
  async create(mapping) {
    logger.info('mappings:create', { uuid: mapping.uuid, description: mapping.description, isPublic: mapping.isPublic });
    const result = await this.raw_api.createMapping({ 
      mappingCreateRequest: mapping
    }).catch((error) => {
      logger.error('mappings:create:failed', { uuid: mapping.uuid, error });
      return Promise.reject(error);
    });
    const _mapping = result.mapping;
    logger.info('mappings:create:done', { id: (_mapping || {}).mappingId, _uuid:  (_mapping || {}).uuid });
    return _mapping;
  }
  /**
   * @param {api.Mapping} mapping 
   */
  async update(mapping) {
    logger.info('mappings:update', { uuid: mapping.uuid, description: mapping.description, isPublic: mapping.isPublic });
    const result = await this.raw_api.updateMapping({ 
      mappingId: mapping.mappingId,
      mappingUpdateRequest: {
        description: mapping.description,
        mappings: mapping.mappings
      }
    }).catch((error) => {
      logger.error('mappings:update:failed', { uuid: mapping.uuid, error });
      return Promise.reject(error);
    });
    const success = result.success;
    logger.info('mappings:update:done', { success, uuid: mapping.uuid, description: mapping.description, isPublic: mapping.isPublic });
    return mapping;
  }
}

module.exports = {
  Mappings
}