const { APIAccessor, logger } = require('../accessor');
APIAccessor;

class Returns {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }

  async * iterator(opt) {
    opt = opt || { };
    opt.skip = opt.skip || 0;
    opt.count = opt.count || 50;
    logger.info('returns:iterator', { opt });

    const hash = { current: {}, previous: {} };
    while (!opt._stop) {
      const returns = await this.list(opt);
      for (let return_ of returns) {
        const id = return_.returnId;
        if (hash.previous[id]) {
          continue;
        }

        const r = await (yield return_);
        r;

        hash.current[id] = 1;
      }

      hash.previous = hash.current;
      hash.current = {};

      if (opt._next) {
        opt._next(opt, returns)
      } else if (opt.order === 'updatedAt') {
        const last = returns[returns.length - 1] || {};
        opt.updatedSince = last.updatedAt;
        opt.skip = 0;
      } else {
        opt.skip = opt.skip + opt.count;
      }

      opt._stop = opt._stop || (returns.length < opt.count)
    }

    logger.info('returns:iterator:done');
  }
  async list(opt) {
    opt = opt || { };
    logger.info('returns:list', { opt });
    const result = await this.raw_api.listReturns({ ...opt }).catch((error) => {
      logger.error('returns:list:failed', { opt, error });
      return Promise.reject(error);
    });
    const returns = result.returns;
    logger.info('returns:list:done', { opt, count: returns.length });
    return returns;
  }
  /**
   * @param {string|{ returnId: string }} id
   */
  async fetch(id) {
    const query = (typeof (id) === 'string') ? { returnId: id } : id ;
    logger.info('returns:fetch', { query });
    const result = await this.raw_api.fetchReturn(query).catch((error) => {
      if (error && error.failure && error.failure.code === 404) {
        return { return: null };
      }
      logger.error('returns:fetch:failed', { query, error });
      return Promise.reject(error);
    });
    const return_ = result.return;
    logger.info('returns:fetch:done', { query, return: return_ });
    return return_;
  }
  /**
   * @param {import('@pipe17/api-client').Return} item
   */
  async upsert(item) {
    logger.info('returns:upsert', { item });
    if (!item.returnId) {
      const result = await this.raw_api.createReturn({
        returnCreateRequest: item
      }).catch((error) => {
        logger.error('returns:create:failed', { item, error });
        return Promise.reject(error)
      });
      item.returnId = result.returns[0].return.returnId;
    } else {
      const result = await this.raw_api.updateReturn({
        returnId: item.returnId,
        returnUpdateRequest: item
      }).catch((error) => {
        logger.error('returns:update:failed', { item, error });
        return Promise.reject(error)
      });
      result;
    }

    logger.info('returns:upsert:done', { item });
    return item;
  }
}

module.exports = {
  Returns
}