const { APIAccessor, logger } = require('../accessor');
APIAccessor;

class Exceptions {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.pvt_api;
  }

  async * iterator(opt) {
    opt = opt || { };
    opt.skip = opt.skip || 0;
    opt.count = opt.count || 50;
    logger.info('exceptions:iterator', { opt });

    const hash = { current: {}, previous: {} };
    while (!opt._stop) {
      const exceptions = await this.list(opt)
      for (let exception of exceptions) {
        const id = exception.exceptionId;
        if (hash.previous[id]) {
          continue;
        }

        const r = await (yield exception);
        r;

        hash.current[id] = 1;
      }

      hash.previous = hash.current;
      hash.current = {};

      if (opt._next) {
        opt._next(opt, exceptions);
      } else if (opt.order === 'updatedAt') {
        const last = exceptions[exceptions.length - 1] || {};
        opt.updatedSince = last.updatedAt;
        opt.skip = 0;
      } else {
        opt.skip = opt.skip + opt.count;
      }

      opt._stop = opt._stop || (exceptions.length < opt.count);
    }

    logger.info('exceptions:iterator:done');
  }
  async list(opt) {
    opt = opt || { };
    logger.info('exceptions:list', { opt });
    const result = await this.raw_api.listExceptions({ ...opt }).catch((error) => {
      logger.error('exceptions:list:failed', { opt, error });
      return Promise.reject(error)
    });
    const exceptions = result.exceptions;
    logger.info('exceptions:list:done', { exceptions });
    return exceptions;
  }
  /**
   * @param {string|{ exceptionId: string }} id
   */
  async fetch(id) {
    const query = (typeof (id) === 'string') ? { exceptionId: id } : id ;
    logger.info('exceptions:fetch', { query });
    const result = await this.raw_api.fetchException(query).catch((error) => {
      logger.error('exceptions:fetch:failed', { query, error });
      return Promise.reject(error)
    });
    const exception = result.exception;
    logger.info('exceptions:fetch:done', { exception });
    return exception;
  }
  /**
   * @param {import('@pipe17/api-client').Exception} item
   */
  async upsert(item) {
    logger.info('exceptions:upsert', { item });

    if (!item.exceptionId) {
      const result = await this.raw_api.createException({
        exceptionCreateRequest: item
      }).catch((error) => {
        logger.error('exceptions:create:failed', { error });
        return Promise.reject(error)
      });
      item.exceptionId = result.exception.exceptionId;
    } else {
      const result = await this.raw_api.updateException({
        exceptionId: item.exceptionId,
        exceptionUpdateRequest: item
      }).catch((error) => {
        logger.error('exceptions:update:failed', { error });
        return Promise.reject(error)
      });
      result;
    }
   
    logger.info('exceptions:upsert:done', { item });
    return item;
  }
}

module.exports = {
  Exceptions
}