const { APIAccessor, logger } = require('../accessor');
APIAccessor;

class Orders {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }

  async * iterator(opt) {
    opt = opt || { };
    opt.skip = opt.skip || 0;
    opt.count = opt.count || 50;
    logger.info('orders:iterator', { opt });

    const hash = { current: {}, previous: {} };
    while (!opt._stop) {
      const orders = await this.list(opt)
      for (let order of orders) {
        const id = order.orderId;
        if (hash.previous[id]) {
          continue;
        }

        const r = await (yield order);
        r;

        hash.current[id] = 1;
      }

      hash.previous = hash.current;
      hash.current = {};

      if (opt._next) {
        opt._next(opt, orders)
      } else if (opt.order === 'updatedAt') {
        const last = orders[orders.length - 1] || {};
        opt.updatedSince = last.updatedAt;
        opt.skip = 0;
      } else {
        opt.skip = opt.skip + opt.count;
      }

      opt._stop = opt._stop || (orders.length < opt.count)
    }

    logger.info('orders:iterator:done');
  }

  async list(opt) {
    opt = opt || { };
    logger.info('orders:list', { opt });
    const result = await this.raw_api.listOrders({ ...opt }).catch((error) => {
      logger.error('orders:list:failed', { opt, error });
      return Promise.reject(error);
    });
    const orders = result.orders;
    logger.info('orders:list:done', { orders });
    return orders;
  }
  /**
   * @param {string|{ orderId: string }} id
   */
  async fetch(id) {
    const query = (typeof (id) === 'string') ? { orderId: id } : id ;
    logger.info('orders:fetch', { query });
    const result = await this.raw_api.fetchOrder(query).catch((error) => {
      if (error && error.failure && error.failure.code === 404) {
        return { order: null };
      }
      logger.error('orders:fetch:failed', { query, error });
      return Promise.reject(error);
    });
    const order = result.order;
    logger.info('orders:fetch:done', { query, order });
    return order;
  }

  /**
   * @param {import('@pipe17/api-client').Order} item
   */
  async upsert(item) {
    logger.info('orders:upsert', { item });

    let existing = null;
    if (item.extOrderId) {
      existing = await this.fetch({ orderId: "ext:" + item.extOrderId }).catch((e) => null);
      if (existing) {
        item.orderId = existing.orderId;
      }
    }

    if (!item.orderId) {
      const result = await this.raw_api.createOrders({
        orderCreateData: [item]
      }).catch((error) => {
        logger.error('orders:create:failed', { item, error });
        return Promise.reject(error);
      });
      item.orderId = result.orders[0].order.orderId;
    } else {
      const result = await this.raw_api.updateOrder({
        orderId: item.orderId,
        orderUpdateRequest: item
      }).catch((error) => {
        logger.error('orders:update:failed', { item, error });
        return Promise.reject(error);
      });
      result;
    }
    logger.info('orders:upsert:done', { item });
    return item;
  }
}

module.exports = {
  Orders
}