const { APIAccessor } = require('../accessor');
APIAccessor;

class Schema {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.schema_api;
  }

  /**
   * @param {any} item 
   * @param {string} mappingId 
   */
  async convert(item, mappingId) {
    const result = await this.raw_api.convert({ convertRequest: { source: item, rules: { mappingId: mappingId } } });
    return result.result.body;
  }

  /**
   * @param {any} item 
   * @param {any[]} rules 
   */
  async convertWithJSON(item, mappings) {
    const result = await this.raw_api.convert({ convertRequest: { source: item, rules: { mappings: mappings } } });
    return result.result.body;
  }
}

module.exports = {
  Schema
}