const { APIAccessor, logger } = require('../accessor');
APIAccessor;

class Shipments {
  /**
   * @param {APIAccessor} api
   */
  constructor(api) {
    this.raw_api = api.raw_api;
  }

  async * iterator(opt) {
    opt = opt || { };
    opt.skip = opt.skip || 0;
    opt.count = opt.count || 50;
    logger.info('shipments:iterator', { opt });

    const hash = { current: {}, previous: {} };
    while (!opt._stop) {
      const shipments = await this.list(opt);
      for (let shipment of shipments) {
        const id = shipment.shipmentId;
        if (hash.previous[id]) {
          continue;
        }

        const r = await (yield shipment);
        r;

        hash.current[id] = 1;
      }

      hash.previous = hash.current;
      hash.current = {};

      if (opt._next) {
        opt._next(opt, shipments)
      } else if (opt.order === 'updatedAt') {
        const last = shipments[shipments.length - 1] || {};
        opt.updatedSince = last.updatedAt;
        opt.skip = 0;
      } else {
        opt.skip = opt.skip + opt.count;
      }

      opt._stop = opt._stop || (shipments.length < opt.count)
    }

    logger.info('shipments:iterator:done');
  }
  async list(opt) {
    opt = opt || { };
    logger.info('shipments:list', { opt });
    const result = await this.raw_api.listShipments({ ...opt }).catch((error) => {
      if ((error.isP17Failure) && (error.failure.code === 404)) {
        return { shipments: [] };
      }
      throw error;
    });
    const shipments = result.shipments;
    logger.info('shipments:list:done', { shipments });
    return shipments;
  }
  /**
   * @param {string|{ shipmentId: string }} id
   */
  async fetch(id) {
    const query = (typeof (id) === 'string') ? { shipmentId: id } : id ;
    logger.info('shipments:fetch', { query });
    const result = await this.raw_api.fetchShipment(query);
    const shipment = result.shipment;
    logger.info('shipments:fetch:done', { shipment });
    return shipment;
  }
  /**
   * @param {import('@pipe17/api-client').Shipment} item
   */
  async upsert(item) {
    logger.info('shipments:upsert', { item });
    // const existing = await this.list({ extorderid: item.extOrderId });
    // if (existing.length) {
    //   item.shipmentId = existing[0].shipmentId;
    // }

    if (!item.shipmentId) {
      const result = await this.raw_api.createShipments({
        shipmentCreateData: [item]
      });
      item.shipmentId = result.shipments[0].shipment.shipmentId;
    } else {
      const result = await this.raw_api.updateShipment({
        shipmentId: item.shipmentId,
        shipmentUpdateRequest: item,
      });
      result;
    }

    logger.info('shipments:upsert:done', { item });
    return item;
  }
}

module.exports = {
  Shipments
}