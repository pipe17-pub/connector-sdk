const logger = require('../common/logger').defaultLogger.child({ module: 'p17-api' });

module.exports = logger;