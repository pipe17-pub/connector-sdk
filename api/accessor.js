const logger = require('./logger')
const api = require('@pipe17/api-client');
const private_api = require('@pipe17/api-client/dist/apis/P17PrivateApi');

function chunk(array, size) {  
  var result = [];
  var chunk = size || 10;
  for (let i = 0; i < array.length; i += chunk) {
    result.push(array.slice(i, i + chunk));          
  }
  return result;
}

class APIAccessor {
  /**
   * @param {Object} configuration
   * @param {import('../configuration/api').APIConfiguration} configuration.api
   * @param {import('../configuration/api').APIConfiguration} configuration.schema_api
   */
  constructor(configuration) {
    logger.info('ctor', { configuration });
    this.raw_api = new api.P17Api(new api.Configuration({...configuration.api}))
    this.pvt_api = new private_api.P17PrivateApi(new api.Configuration({...configuration.api}))

    this.schema_api = new api.ConvertApi(new api.Configuration({...configuration.schema_api, headers: { debug: true }}))
  }
}

module.exports = {
  APIAccessor, logger, chunk
}