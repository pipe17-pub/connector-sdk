const api = require('@pipe17/api-client');
api;

const fs = require('fs');
const path = require('path');

class Mappings {
  /**
   * @param {(api.Entity & { mappingRules: string })[]} [mappings=null]
   */
  constructor(mappings) {
    /** @type {(api.Entity & { mappingRules: string })[]} */
    this.mappings = mappings || [];
  }

  /**
   * @param {api.EntityName} entityType 
   * @param {api.Direction} direction 
   */
  findMapping(entityType, direction) {
    const found = this.mappings.filter((x) => x.name === entityType).filter(x => x.direction === direction);
    if (!found.length) {
      return null;
    }
    return found[0];
  }
  /**
   * @param {api.EntityName} entityType 
   * @param {api.Direction} direction 
   */
  findMappingId(entityType, direction) {
    return this.findMapping(entityType, direction)
  }

  /**
   * @param {api.EntityName} entityType 
   * @param {api.Direction} direction 
   * @param {string} mappingId 
   */
  withMapping(entityType, direction, mappingId) {
    const mapping = this.findMapping(entityType, direction);
    if (mapping) {
      mapping.mappingUuid = mappingId;
    } else {
      this.mappings.push({ name: entityType, direction: direction, mappingUuid: mappingId })
    }
    return this;
  }

  /**
   * @param {string} location 
   */
  loadMappings(location) {
    if (this._loaded) {
      return;
    }
    this.mappings.forEach((x) => {
      try {
        const fr = path.join(location, x.mappingUuid) + '.json';
        const sr = fs.readFileSync(fr, { encoding: 'utf-8' });
        const or = JSON.parse(sr);
        x.mappingRules = or;
      } catch (e) {
        x.mappingRules = { };
      }
    });
    this._loaded = true;
  }
}


module.exports = {
  Mappings
}