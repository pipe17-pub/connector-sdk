const api = require('@pipe17/api-client');
api;

class Settings {
  /**
   * @param {api.ConnectorSettingsField[]} [settings=null]
   */
  constructor(fields) {
    /** @type {api.ConnectorSettingsField[]} */
    this.fields = fields || [];
  }

  /**
   * @param {string} path 
   */
  getSetting(path) {
    const found = this.fields.filter((x) => x.path === path);
    return found[0] || null;
  }
  /**
   * @param {string} path 
   */
  getValue(name) {
    return (this.getSetting(name) || {}).value;
  }
  /**
   * @param {string} path 
   */
  setValue(path, value) {
    (this.getSetting(path) || {}).value = value;
  }

  /**
   * @param {string} path 
   * @param {api.ConnectorSettingsFieldTypeEnum} type 
   * @param {boolean} secret 
   * @param {api.ConnectorSettingsOptions} options? = { } 
   */
  withSetting(path, type, secret, options) {
    options = options || {};
    const setting = this.getSetting(path);
    if (setting) {
      setting.type = type;
      setting.secret = secret;
      setting.options = options;
    } else {
      this.fields.push({ path, type, secret, options });
    }
    this.setValue(path, options.defaultValue || '')
    return this;
  }

  clone() {
    const settings = this.fields.map(x => ({...x}));
    return new Settings(settings);
  }
}

class ConnectionSettings extends Settings {
  /**
   * @param {api.ConnectorSettingsField[]} [fields=null]
   * @param {string} [type]
   * @param {string} [url]
   */
  constructor(fields, type, url) {
    super(fields);
    this.type = type;
    this.url = url;
  }

  clone() {
    return new ConnectionSettings(super.clone().fields, this.type, this.url)
  }
}


module.exports = {
  Settings,
  ConnectionSettings
}