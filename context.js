const { API } = require('./api');
const { TriggerTypes } = require('./meta/triggers');
const { ConnectorEvents } = require('./events');

/**
 * @typedef {Object} Args
 * @prop {string} [integrationId]
 * @prop {import('./meta/triggers').TriggerType[]} [triggerTypes]
 * @prop {import('./api').EntityType[]} [entityTypes]
 * @prop {string} [entityTypesFilter]
 * @prop {Object} [entityTypesSettings]
 * @prop {Object} [eventInfo]
 * @prop {string} [requestId]
 */

 /**
 * @typedef {Object} Context
 * @prop {API} [api]
 * @prop {import('./connector').Connector} [connector]
 * @prop {import('./configuration').Configuration} [configuration]
 * @prop {Args} [args]
 * @prop {TriggerType[]} [triggerTypes]
 * @prop {TriggerType[]} [entityTypes]
 * @prop {import('@pipe17/api-client').Integration} [integration]  
 * @prop {import('./events').ConnectorEvents} [eventReporter]  
 */

class ConnectorContext {
  /**
   * @param {import('./connector').Connector} connector 
   */
  constructor(connector) {
    /** @type {import('./connector').Connector} */
    this.connector = connector;

    /** @type {API} */
    this.api = null;
    this.configuration = connector.configuration;
    /** @type {Args} */
    this.args = null;
    /** @type {TriggerType[]} */
    this.triggerTypes = [TriggerTypes.P17Poll];
    this.entityTypes = ['integrations'];
    this.eventInfo = { };

    this.eventReporter = new ConnectorEvents(this);
  }

  /**
   * @private 
   * @param {Args} args
   * @returns undefined
   */
  async _prepareP17webhook(args) {
    if ('string' != typeof args.eventRaw) {
      return Promise.reject('eventRaw parameter is missed or not a string');
    }
    try {
      const eventInfo = JSON.parse(args.eventRaw);
      const integrationId = eventInfo.integration || (eventInfo.entity === 'integrations' && eventInfo.itemId);
      eventInfo.integration = eventInfo.integration || integrationId;
      args.eventInfo = eventInfo;
      args.integrationId = integrationId;
    }
    catch (error) {
      return Promise.reject('Cannot parse eventRaw');
    }
    if (!args.eventInfo.entity) return Promise.reject('No entity');
    if (!args.integrationId) return Promise.reject('No integration');
  }

  /**
   * @param {Args} args 
   * @returns {Context}
   */
  async prepare(args) {
    this.args = args;
    const _api = new API( { 
      api: this.connector.configuration.api_configuration,
      schema_api: this.connector.configuration.schema_api_configuration
    });
    if (args.triggerTypes && Array.isArray(args.triggerTypes) && args.triggerTypes.includes(TriggerTypes.P17Event)) {
      await this._prepareP17webhook(args);
    }
    if (!args.integrationId) {
      this.api = _api;
      this.triggerTypes = [TriggerTypes.P17Poll];
      this.entityTypes = ['integrations'];
      this.concurrent = true;
    } else {
      const integration = await _api.integrations.fetch(args.integrationId);

      const configuration = this.connector.configuration.clone();
      configuration.mappings.mappings = integration.entities;

      configuration.settings.fields = (integration.settings || {}).fields || [];
      configuration.connectionSettings.fields = (integration.connection || {}).fields || [];

      if (!this.connector.configuration.api_configuration.forceKey) {
        configuration.api_configuration.apiKey = integration.apikey;
      }
      this.integration = integration;
      this.api = new API({ 
        api: configuration.api_configuration,
        schema_api: configuration.schema_api_configuration
      });

      // need this because only connector key can apply updates to integration itself;
      this.integrationAPI = _api.integrations;
      this.connectorAPI = _api.connectors;
      this.configuration = configuration;

      const triggerTypes = args.triggerTypes;
      if (!triggerTypes) {
        this.triggerTypes = [TriggerTypes.VendorPoll];
      } else {
        const metaTypes = Object.getOwnPropertyNames(TriggerTypes).reduce((r, x) => {
          const type = TriggerTypes[x];
          r[type.name] = type;
          return r;
        }, {});
        const types = triggerTypes.map((x) => metaTypes[x.name]).filter(x => !!x);
        this.triggerTypes = [...types];
      }

      const entityTypesFilter = (args.entityTypesFilter || '').split(',').filter((x) => !!x);
      const entityTypes = args.entityTypes ? args.entityTypes.filter((e) => (!entityTypesFilter.length || entityTypesFilter.includes(e) || entityTypesFilter.includes('*'))) : undefined;
      this.entityTypes = entityTypes;
      this.eventInfo = args.eventInfo;
    }
    return {...this};
  }
}

module.exports = {
  ConnectorContext
}